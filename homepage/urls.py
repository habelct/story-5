from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('todo/', views.todo, name='todo'),
    path('todo/create', views.todo_create, name='todo_create'),
    path('todo/delete', views.todo_delete, name='todo_delete'),
    path('todo/deletesatu/<int:id>', views.todo_deletesatu, name='todo_deletesatu'),
]